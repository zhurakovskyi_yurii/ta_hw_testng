package com.epam.homeworks.tariffs.project.model;

public class Non_Limit_Internet extends Tariff {
    public Non_Limit_Internet() {
        this.setId(3);
        this.setName("Non-Limit Internet");
        this.setPayroll(220);
        this.setCalls_on_net_cost(1.20);
        this.setSms_within_ukraine_cost(0.50);
        this.setInternet(5000);
        this.setLand_lines(2.50);
    }
}
