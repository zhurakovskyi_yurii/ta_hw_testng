package com.epam.homeworks.tariffs.project.dao;

import com.epam.homeworks.tariffs.project.model.CallEntity;

import java.sql.SQLException;
import java.util.List;

public interface CallDAO extends  GeneralDAO<CallEntity>{
    List<CallEntity> findAll() throws SQLException;

    void delete(int cuser_from, int cuser_to) throws SQLException;

    CallEntity create(CallEntity entity) throws SQLException;

    CallEntity findById(int cuser_from) throws SQLException;
}
