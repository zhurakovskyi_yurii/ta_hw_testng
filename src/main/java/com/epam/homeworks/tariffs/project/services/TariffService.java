package com.epam.homeworks.tariffs.project.services;

import com.epam.homeworks.tariffs.project.dao.TariffDAO;
import com.epam.homeworks.tariffs.project.dao.implementation.TariffDAOImpl;
import com.epam.homeworks.tariffs.project.model.Tariff;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class TariffService {
    private static Logger logger = LogManager.getLogger(TariffService.class);

    public List<Tariff> findAll() {
        List<Tariff> tariffEntities = new ArrayList<>();
        try {
            tariffEntities = new TariffDAOImpl().findAll();
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return tariffEntities;
    }

    public Optional<Tariff> findById(int id) {
        Optional<Tariff> tariffEntity = Optional.empty();
        TariffDAO tariffDAO = new TariffDAOImpl();
        try {
            tariffEntity = tariffDAO.findById(id);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return tariffEntity;
    }

    public void update(Tariff tariff) {
        try {
            new TariffDAOImpl().update(tariff);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public Tariff create(Tariff tariff) {
        try {
            tariff = new TariffDAOImpl().create(tariff);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return tariff;
    }

    public void delete(int id) {
        try {
            new TariffDAOImpl().delete(id);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void deleteByName(String name){
        try {
            new TariffDAOImpl().deleteByName(name);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public Optional<Tariff> findByName(String name) {
        Optional<Tariff> tariffEntity = Optional.empty();
        try {
            tariffEntity = new TariffDAOImpl().findByName(name);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return tariffEntity;
    }

    public void deleteAll() {
        try {
            new TariffDAOImpl().deleteAll();
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }
}
