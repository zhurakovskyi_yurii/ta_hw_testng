package com.epam.homeworks.tariffs.project.model;

import java.sql.Date;
import java.util.Objects;

public class AccountEntity {
    private int id;
    private String phone_number;
    private double balance;
    private Date last_replenishment_date;
    private Date active_to;

    public AccountEntity() {
    }

    public AccountEntity(int id, String phone_number, double balance, Date last_replenishment_date, Date active_to) {
        this.id = id;
        this.phone_number = phone_number;
        this.balance = balance;
        this.last_replenishment_date = last_replenishment_date;
        this.active_to = active_to;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Date getLast_replenishment_date() {
        return last_replenishment_date;
    }

    public void setLast_replenishment_date(Date last_replenishment_date) {
        this.last_replenishment_date = last_replenishment_date;
    }

    public java.sql.Date getActive_to() {
        return active_to;
    }

    public void setActive_to(Date active_to) {
        this.active_to = active_to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountEntity that = (AccountEntity) o;
        return Objects.equals(phone_number, that.phone_number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phone_number);
    }

    @Override
    public String toString() {
        return "AccountEntity{" +
                "id=" + id +
                ", phone_number='" + phone_number + '\'' +
                ", balance=" + balance +
                ", last_replenishment_date=" + last_replenishment_date +
                ", active_to=" + active_to +
                '}';
    }
}
