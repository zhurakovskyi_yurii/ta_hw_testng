package com.epam.homeworks.tariffs.project.dao;

import java.sql.SQLException;
import java.util.List;

public interface GeneralDAO<T> {
    List<T> findAll() throws SQLException;

    T create(T entity) throws SQLException;

    int deleteAll() throws SQLException;
}
