DROP DATABASE IF EXISTS mobile_service;
CREATE DATABASE mobile_service;
USE mobile_service;
SELECT database();

create table tariffs(
	id int not null primary key auto_increment,
    name varchar(120) not null,
    payroll double not null,
    calls_on_net_cost double not null,
    sms_within_ukraine_cost double not null,
    internet int not null,
    land_lines double not null
);

create table accounts(
	id int not null primary key auto_increment,
	phone_number varchar(160) not null,
    balance double not null,
    replenishment_date date not null,
	active_to date
);

create table users(
	id int not null primary key auto_increment,
    phone_number varchar(20) not null,
    first_name varchar(120) not null,
    last_name varchar(120) not null,
    tariff_name varchar(120) not null
);

alter table users add foreign key (tariff_name) references tariffs(name);
alter table users add foreign key (phone_number) references accounts(phone_number);

create table sms(
	sms_id int not null auto_increment,
	user_from int not null,
    user_to int not null,
    send_date date not null,
    text varchar(120),
    primary key (sms_id)
);

alter table sms add foreign key(user_from) references users(id);
alter table sms add foreign key(user_to) references users(id); 

create table calls(
	call_id int not null auto_increment,
	cuser_from int not null, 
    cuser_to int not null,
    send_date date not null,
    primary key (call_id)
);

alter table calls add foreign key(cuser_from) references users(id);
alter table calls add foreign key(cuser_to) references users(id);

select*from tariffs;