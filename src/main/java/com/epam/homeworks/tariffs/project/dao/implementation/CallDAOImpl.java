package com.epam.homeworks.tariffs.project.dao.implementation;

import com.epam.homeworks.tariffs.project.dao.CallDAO;
import com.epam.homeworks.tariffs.project.model.CallEntity;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CallDAOImpl implements CallDAO {
    private static final String FIND_ALL = "SELECT * FROM calls";
    private static final String DELETE = "DELETE FROM calls WHERE cuser_from = ? AND cuser_to =?";
    private static final String CREATE = "INSERT INTO calls(cuser_from, cuser_to, send_date) VALUES (?, ?, ?)";
    private static final String FIND_BY_CUSER_FROM = "SELECT * FROM account cuser_from id = ?";


    public List<CallEntity> findAll() throws SQLException {
        List<CallEntity> callEntities = new ArrayList<>();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_ALL)) {
            preparedStatement.execute();
            try (ResultSet resultSet = preparedStatement.getResultSet()) {
                while (resultSet.next()) {
                    int user_from = resultSet.getInt("cuser_from");
                    int user_to = resultSet.getInt("cuser_to");
                    Date date = resultSet.getDate("_date");
                    CallEntity callEntity = new CallEntity();
                    callEntity.setCuser_from(user_from);
                    callEntity.setCuser_to(user_to);
                    callEntity.setDate(date);
                    callEntities.add(callEntity);
                }
            }
        }
        return callEntities;
    }

    public CallEntity findById(int cuser_from) throws SQLException {
        CallEntity callEntity = null;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_CUSER_FROM)) {
            preparedStatement.setInt(1, cuser_from);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.next();
                int user_from = resultSet.getInt("cuser_from");
                int user_to = resultSet.getInt("cuser_to");
                Date date = resultSet.getDate("_date");
                callEntity.setCuser_from(user_from);
                callEntity.setCuser_to(user_to);
                callEntity.setDate(date);
            }
        }
        return callEntity;
    }


    public CallEntity create(CallEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(CREATE,
                Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, entity.getCuser_from());
            preparedStatement.setInt(2, entity.getCuser_to());
            preparedStatement.setDate(3, new java.sql.Date(entity.getDate().getTime()));
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                resultSet.next();
                entity.setId(resultSet.getInt(1));
            }
        }
        return entity;
    }

    @Override
    public int deleteAll() throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE)) {
            return preparedStatement.executeUpdate();
        }
    }

    public void delete(int cuser_from, int cuser_to) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE)) {
            preparedStatement.setInt(1, cuser_from);
            preparedStatement.setInt(2, cuser_to);
            preparedStatement.executeUpdate();
        }
    }
}
