package com.epam.homeworks.tariffs.project.services;

import com.epam.homeworks.tariffs.project.dao.implementation.*;
import com.epam.homeworks.tariffs.project.model.*;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SMSService {
    private static Logger logger = LogManager.getLogger(SMSService.class);

    public void create(SMS sms) {
        try {
            new SMSDAOImpl().create(sms);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public List<SMS> findAll() {
        List<SMS> smsEntities = null;
        try {
            smsEntities = new SMSDAOImpl().findAll();
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return smsEntities;
    }

    public SMS findByUserFrom(int user_from) {
        SMS sms = null;
        try {
            new SMSDAOImpl().findById(user_from);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return sms;
    }

    public void delete(int user_from) {
        try {
            new SMSDAOImpl().delete(user_from);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

}