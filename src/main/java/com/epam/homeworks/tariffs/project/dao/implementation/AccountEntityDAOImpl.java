package com.epam.homeworks.tariffs.project.dao.implementation;

import com.epam.homeworks.tariffs.project.dao.AccountEntityDAO;
import com.epam.homeworks.tariffs.project.model.AccountEntity;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountEntityDAOImpl implements AccountEntityDAO<AccountEntity> {
    private static final String FIND_ALL = "SELECT * FROM accounts";
    private static final String DELETE = "DELETE FROM accounts WHERE id = ?";
    private static final String DELETE_ALL = "DELETE FROM accounts";
    private static final String CREATE = "INSERT INTO accounts(phone_number, balance, replenishment_date, active_to) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE accounts SET phone_number =?, balance =?, replenishment_date=?, active_to=? WHERE id = ?";
    private static final String FIND_BY_ID = "SELECT * FROM accounts WHERE id = ?";
    private static final String FIND_BY_PHONE = "SELECT * FROM accounts WHERE phone_number=? ";
    private static final String DELETE_BY_PHONE = "DELETE FROM accounts WHERE phone_number= ?";

    public List findAll() throws SQLException {
        List<AccountEntity> accountEntities = new ArrayList<>();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_ALL)) {
            preparedStatement.execute();
            try (ResultSet resultSet = preparedStatement.getResultSet()) {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String phone_number = resultSet.getString("phone_number");
                    double balance = resultSet.getDouble("balance");
                    Date last_replenishment_date = resultSet.getDate("replenishment_date");
                    Date active_to = resultSet.getDate("active_to");
                    AccountEntity accountEntity = new AccountEntity(id, phone_number, balance, last_replenishment_date, active_to);
                    accountEntities.add(accountEntity);
                }
            }
        }
        return accountEntities;
    }

    public AccountEntity findById(int id) throws SQLException {
        AccountEntity accountEntity = null;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_ID)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.next();
                int _id = resultSet.getInt("id");
                String phone_number = resultSet.getString("phone_number");
                double balance = resultSet.getDouble("balance");
                Date last_replenishment_date = resultSet.getDate("replenishment_date");
                Date active_to = resultSet.getDate("active_to");
                accountEntity = new AccountEntity(id, phone_number, balance, last_replenishment_date, active_to);
            }
        }
        return accountEntity;
    }

    public Optional<AccountEntity> findByPhone(String phone) throws SQLException {
        Optional<AccountEntity> accountEntity = null;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_PHONE)) {
            preparedStatement.setString(1, phone);
            preparedStatement.execute();
            try (ResultSet resultSet = preparedStatement.getResultSet()) {
                resultSet.next();
                int _id = resultSet.getInt("id");
                String phone_number = resultSet.getString("phone_number");
                double balance = resultSet.getDouble("balance");
                Date last_replenishment_date = resultSet.getDate("replenishment_date");
                Date active_to = resultSet.getDate("active_to");
                accountEntity = Optional.ofNullable(new AccountEntity(_id, phone_number, balance, last_replenishment_date, active_to));
            }
        }
        return accountEntity;
    }

    @Override
    public AccountEntity findByUser(int user_id) throws SQLException {
        AccountEntity accountEntity = null;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_ID)) {
            preparedStatement.setInt(1, user_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.next();
                int id = resultSet.getInt("id");
                String phone_number = resultSet.getString("phone_number");
                double balance = resultSet.getDouble("balance");
                Date last_replenishment_date = resultSet.getDate("replenishment_date");
                Date active_to = resultSet.getDate("active_to");
                accountEntity = new AccountEntity(id, phone_number, balance, last_replenishment_date, active_to);
            }
        }
        return accountEntity;
    }

    public AccountEntity create(AccountEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(CREATE,
                Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, entity.getPhone_number());
            preparedStatement.setDouble(2, entity.getBalance());
            preparedStatement.setDate(3, entity.getLast_replenishment_date());
            preparedStatement.setDate(4, entity.getActive_to());
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                resultSet.next();
                entity.setId(resultSet.getInt(1));
            }
        }
        return entity;
    }

    public void update(AccountEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getPhone_number());
            preparedStatement.setDouble(2, entity.getBalance());
            preparedStatement.setDate(3, entity.getLast_replenishment_date());
            preparedStatement.setDate(4, entity.getActive_to());
            preparedStatement.setInt(5, entity.getId());
            preparedStatement.executeUpdate();
        }
    }

    public int delete(int id) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }

    public int deleteAll() throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE_ALL)) {
            return preparedStatement.executeUpdate();
        }
    }

    public void deleteByPhone(String phone) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE_BY_PHONE)) {
            preparedStatement.setString(1, phone);
            preparedStatement.executeUpdate();
        }

    }
}
