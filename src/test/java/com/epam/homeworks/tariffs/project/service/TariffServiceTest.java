package com.epam.homeworks.tariffs.project.service;

import com.epam.homeworks.tariffs.project.model.Tariff;
import com.epam.homeworks.tariffs.project.services.TariffService;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Optional;

import static org.testng.Assert.assertEquals;

public class TariffServiceTest {
    private final TariffService tariffService = new TariffService();
    private Tariff tariff;

    private Tariff create() {
        tariff = new Tariff();
        tariff.setName("tariff name");
        tariff = tariffService.create(tariff);
        return tariff;
    }

    @Test
    public void createTest() {
        tariffService.deleteAll();
        tariff = create();
        assertEquals(tariffService.findAll(), Collections.singletonList(tariff));
        tariffService.deleteAll();
    }

    @Test
    public void updateTest() {
        tariff = create();
        tariff.setName("tariff name1");
        tariffService.update(tariff);
        assertEquals(tariffService.findById(tariff.getId()), Optional.of(tariff));
        tariffService.deleteAll();
    }

    @Test
    public void deleteTest() {
        tariff = create();
        tariffService.delete(tariff.getId());
        assertEquals(tariffService.findAll(), Collections.emptyList());
        tariffService.deleteAll();
    }

    @Test
    public void findAllTest() {
        tariff = create();
        assertEquals(tariffService.findAll(), Collections.singletonList(tariff));
        tariffService.deleteAll();
    }

    @Test
    public void findByIdTest() {
        tariff = create();
        assertEquals(tariffService.findById(tariff.getId()), Optional.of(tariff));
        tariffService.deleteAll();
    }

    @Test
    public void findByNameTest() {
        tariff = create();
        assertEquals(tariffService.findByName(tariff.getName()), Optional.of(tariff));
        tariffService.deleteAll();
    }

    @Test
    public void deleteAllTest() {
        tariff = create();
        tariffService.deleteAll();
        assertEquals(tariffService.findAll(), Collections.emptyList());
    }
}
