package com.epam.homeworks.tariffs.project.dao;

import com.epam.homeworks.tariffs.project.model.SMS;

import java.sql.SQLException;
import java.util.List;

public interface SMSDAO extends GeneralDAO<SMS> {
    List<SMS> findAll() throws SQLException;

    SMS findById(int user_from) throws SQLException;

    SMS create(SMS entity) throws SQLException;

    int delete(int user_from) throws SQLException;
}
