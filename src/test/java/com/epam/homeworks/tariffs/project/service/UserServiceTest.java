package com.epam.homeworks.tariffs.project.service;

import com.epam.homeworks.tariffs.project.model.AccountEntity;
import com.epam.homeworks.tariffs.project.model.Tariff;
import com.epam.homeworks.tariffs.project.model.User;
import com.epam.homeworks.tariffs.project.services.AccountService;
import com.epam.homeworks.tariffs.project.services.TariffService;
import com.epam.homeworks.tariffs.project.services.UserService;
import com.twilio.rest.api.v2010.Account;
import org.testng.annotations.*;

import java.sql.Date;
import java.util.Collections;
import java.util.Optional;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

@Test
public class UserServiceTest {
    private final UserService userService = new UserService();

    private User user;
    private AccountEntity accountEntity;

    private User create() {
        user = new User();
        user.setFirst_name("John");
        user.setLast_name("Smith");
        Tariff tariff = new Tariff();
        tariff.setName("new tariff");
        user.setTariff(tariff);
        accountEntity = new AccountEntity();
        accountEntity.setPhone_number("+380999999999");
        user.setAccount(accountEntity);
        user = userService.create(user);
        return user;
    }

    @Test
    public void createTest() {
        user = create();
        assertEquals(userService.findAll(), Collections.singletonList(user));
        userService.deleteAll();
    }

    @Test
    public void findByNameTest() {
        user = create();
        assertEquals(userService.findByName(user.getFirst_name(), user.getLast_name()), Optional.of(user));
        userService.deleteAll();
    }

    @Test
    public void findAllTest() {
        user = create();
        assertEquals(userService.findAll(), Collections.singletonList(user));
        userService.deleteAll();
    }

    @Test
    public void findByIdTest() {
        user = create();
        System.out.println(user.getId());
        assertEquals(userService.findById(user.getId()), Optional.of(user));
        userService.deleteAll();
    }

    @Test
    public void findByPhoneTest() {
        user = create();
        assertEquals(userService.findByPhone(accountEntity.getPhone_number()), Optional.of(user));
        userService.deleteAll();
    }

    @Test
    public void updateTest() {
        user = create();
        user.setFirst_name("Williams");
        userService.update(user);
        assertEquals(userService.findById(user.getId()), Optional.of(user));
        userService.deleteAll();
    }

    @Test
    public void deleteTest() {
        user = create();
        userService.delete(user.getId());
        assertEquals(userService.findById(user.getId()), Optional.empty());
        userService.deleteAll();
    }

    @Test
    public void deleteByPhoneTest() {
        user = create();
        userService.deleteByPhone(accountEntity.getPhone_number());
        assertEquals(userService.findByPhone(accountEntity.getPhone_number()), Optional.empty());
        userService.deleteAll();
    }
}
