package com.epam.homeworks.tariffs.project.service;

import com.epam.homeworks.tariffs.project.model.AccountEntity;
import com.epam.homeworks.tariffs.project.services.AccountService;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Date;

import static org.testng.Assert.assertEquals;

@Test
public class AccountServiceTest {
    private final AccountService accountService = new AccountService();
    private AccountEntity accountEntity;

    private AccountEntity create() {
        accountEntity = new AccountEntity();
        accountEntity.setPhone_number("+380999999999");
        accountEntity.setBalance(111.11);
        accountEntity.setActive_to(new java.sql.Date(new Date().getTime()));
        accountEntity.setLast_replenishment_date(new java.sql.Date(new Date().getTime()));
        accountEntity = accountService.create(accountEntity);
        return accountEntity;
    }

    @Test
    public void createTest() {
        accountEntity = create();
        assertEquals(accountService.findAll(), Collections.singletonList(accountEntity));
        accountService.deleteAll();
    }

    @Test
    public void updateTest() {
        accountEntity = create();
        accountEntity.setBalance(250.50);
        accountService.update(accountEntity);
        assertEquals(accountService.findAll(), Collections.singletonList(accountEntity));
        accountService.deleteAll();
    }

    @Test
    public void deleteTest() {
        accountEntity = create();
        accountService.delete(accountEntity.getId());
        assertEquals(accountService.findAll(), Collections.emptyList());
        accountService.deleteAll();
    }

    @Test
    public void deleteByPhone() {
        accountEntity = create();
        accountService.deleteByPhone(accountEntity.getPhone_number());
        assertEquals(accountService.findAll(), Collections.emptyList());
        accountService.deleteAll();
    }
}
