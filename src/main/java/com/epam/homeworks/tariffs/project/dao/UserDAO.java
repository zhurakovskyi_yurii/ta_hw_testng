package com.epam.homeworks.tariffs.project.dao;

import com.epam.homeworks.tariffs.project.model.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserDAO extends GeneralDAO<User> {
    List<User> findAll() throws SQLException;

    Optional<User> findById(int id) throws SQLException;

    User create(User entity) throws SQLException;

    void update(User entity) throws SQLException;

    int delete(int id) throws SQLException;

    int deleteAll() throws SQLException;
}
