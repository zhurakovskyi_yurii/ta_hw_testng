package com.epam.homeworks.tariffs.project.services;

import com.epam.homeworks.tariffs.project.dao.CallDAO;
import com.epam.homeworks.tariffs.project.dao.implementation.CallDAOImpl;
import com.epam.homeworks.tariffs.project.model.AccountEntity;
import com.epam.homeworks.tariffs.project.model.CallEntity;
import com.epam.homeworks.tariffs.project.model.Tariff;
import com.epam.homeworks.tariffs.project.model.User;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class CallService {
    private static Logger logger = LogManager.getLogger(CallService.class);

    public CallEntity create(CallEntity callEntity) {
        try {
            CallDAO callDAO = new CallDAOImpl();
            callEntity = callDAO.create(callEntity);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return callEntity;
    }

    public List<CallEntity> findAll() {
        List<CallEntity> callEntities = null;
        try {
            CallDAO callDAO = new CallDAOImpl();
            callEntities = callDAO.findAll();
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return callEntities;
    }

    public CallEntity findByUserFrom(int cuser_from) {
        CallEntity callEntity = null;
        try {
            callEntity = new CallDAOImpl().findById(cuser_from);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return callEntity;
    }

    public void delete(int cuser_from, int cuser_to) {
        try {
            new CallDAOImpl().delete(cuser_from, cuser_to);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }


    public void deleteAll() {
        try {
            new CallDAOImpl().deleteAll();
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }
}
