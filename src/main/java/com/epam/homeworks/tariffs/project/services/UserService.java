package com.epam.homeworks.tariffs.project.services;

import com.epam.homeworks.tariffs.project.dao.UserDAO;
import com.epam.homeworks.tariffs.project.dao.implementation.AccountEntityDAOImpl;
import com.epam.homeworks.tariffs.project.dao.implementation.UserDAOImpl;
import com.epam.homeworks.tariffs.project.model.User;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class UserService {
    private static Logger logger = LogManager.getLogger(UserService.class);

    public Optional<User> findByName(String firstName, String lastName) {
        Optional<User> user = Optional.empty();
        try {
            user = Optional.ofNullable(new UserDAOImpl().findByName(firstName, lastName));
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()) + "\n");
        }
        return user;
    }

    public List<User> findAll() {
        List<User> userEntities = new ArrayList<>();
        try {
            UserDAO userDAO = new UserDAOImpl();
            userEntities = userDAO.findAll();
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()) + "\n");
        }

        return userEntities;
    }

    public Optional<User> findById(int id) {
        Optional<User> userEntity = Optional.empty();
        UserDAO userDAO = new UserDAOImpl();
        try {
            userEntity = userDAO.findById(id);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()) + "\n");
        }
        return userEntity;
    }

    public Optional<User> findByPhone(String phone) {
        Optional<User> userEntity = Optional.empty();
        try {
            userEntity = new UserDAOImpl().findByPhone(phone);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()) + "\n");
        }
        return userEntity;
    }

    public User create(User user) {
        try {
            user = new UserDAOImpl().create(user);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return user;
    }

    public void update(User user) {
        try {
            new UserDAOImpl().update(user);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void delete(int id) {
        try {
            new UserDAOImpl().delete(id);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void deleteByPhone(String phone) {
        try {
            new UserDAOImpl().deleteByPhone(phone);
            new AccountEntityDAOImpl().deleteByPhone(phone);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void deleteAll() {
        try {
            UserDAO userDAO = new UserDAOImpl();
            userDAO.deleteAll();
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }

    }
}

