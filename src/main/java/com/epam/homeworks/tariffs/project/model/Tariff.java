package com.epam.homeworks.tariffs.project.model;

import java.util.Objects;

public class Tariff {
    private int id;
    private String name;
    private double payroll;
    private double calls_on_net_cost;
    private double sms_within_ukraine_cost;
    private int internet;
    private double land_lines;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPayroll(double payroll) {
        this.payroll = payroll;
    }

    public void setCalls_on_net_cost(double calls_on_net_cost) {
        this.calls_on_net_cost = calls_on_net_cost;
    }

    public void setSms_within_ukraine_cost(double sms_within_ukraine_cost) {
        this.sms_within_ukraine_cost = sms_within_ukraine_cost;
    }

    public void setInternet(int internet) {
        this.internet = internet;
    }

    public void setLand_lines(double land_lines) {
        this.land_lines = land_lines;
    }

    public String getName() {
        return name;
    }

    public double getPayroll() {
        return payroll;
    }

    public double getCalls_on_net_cost() {
        return calls_on_net_cost;
    }

    public double getSms_within_ukraine_cost() {
        return sms_within_ukraine_cost;
    }

    public int getInternet() {
        return internet;
    }

    public double getLand_lines() {
        return land_lines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tariff tariff = (Tariff) o;
        return Objects.equals(name, tariff.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "TariffEntity{" +
                " name='" + name + '\'' +
                ", payroll=" + payroll +
                ", calls_on_net_cost=" + calls_on_net_cost +
                ", sms_within_ukraine_cost=" + sms_within_ukraine_cost +
                ", internet=" + internet +
                ", land_lines=" + land_lines +
                '}';
    }
}