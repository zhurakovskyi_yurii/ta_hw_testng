package com.epam.homeworks.tariffs.project.dao;

import com.epam.homeworks.tariffs.project.model.Tariff;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface TariffDAO extends GeneralDAO<Tariff> {
    List<Tariff> findAll() throws SQLException;

    Optional<Tariff> findById(int id) throws SQLException;

    Tariff create(Tariff entity) throws SQLException;

    void update(Tariff entity) throws SQLException;

    void delete(int id) throws SQLException;

    Optional<Tariff> findByName(String name) throws SQLException;
}
