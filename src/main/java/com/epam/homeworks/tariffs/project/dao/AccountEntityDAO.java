package com.epam.homeworks.tariffs.project.dao;

import com.epam.homeworks.tariffs.project.model.AccountEntity;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface AccountEntityDAO<A> extends GeneralDAO<AccountEntity> {
    List findAll() throws SQLException;

    Optional<AccountEntity> findByPhone(String phone) throws SQLException;

    AccountEntity findById(int id) throws SQLException;

    AccountEntity findByUser(int user_id) throws SQLException;

    AccountEntity create(AccountEntity entity) throws SQLException;

    void update(AccountEntity entity) throws SQLException;

    int delete(int id) throws SQLException;

    void deleteByPhone(String phone) throws SQLException;
}
