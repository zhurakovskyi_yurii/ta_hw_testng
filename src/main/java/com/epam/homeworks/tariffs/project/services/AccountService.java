package com.epam.homeworks.tariffs.project.services;

import com.epam.homeworks.tariffs.project.dao.AccountEntityDAO;
import com.epam.homeworks.tariffs.project.dao.implementation.AccountEntityDAOImpl;
import com.epam.homeworks.tariffs.project.model.AccountEntity;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class AccountService {
    private static Logger logger = LogManager.getLogger(AccountService.class);

    public List<AccountEntity> findAll() {
        AccountEntityDAO<AccountEntity> accountEntityDAO;
        List<AccountEntity> accountEntities = new ArrayList<>();
        try {
            accountEntityDAO = new AccountEntityDAOImpl();
            accountEntities = accountEntityDAO.findAll();
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return accountEntities;
    }

    public Optional<AccountEntity> findById(int id) {
        Optional<AccountEntity> accountEntity = null;
        try {
            accountEntity = Optional.ofNullable(new AccountEntityDAOImpl().findById(id));
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return accountEntity;
    }

    public Optional<AccountEntity> findByPhone(String phone) {
        Optional<AccountEntity> accountEntity = Optional.empty();
        try {
            accountEntity = new AccountEntityDAOImpl().findByPhone(phone);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return accountEntity;
    }

    public AccountEntity create(AccountEntity accountEntity) {
        try {
            AccountEntityDAO<AccountEntity> accountEntityDAO = new AccountEntityDAOImpl();
            accountEntity = accountEntityDAO.create(accountEntity);
        } catch (SQLIntegrityConstraintViolationException e) {
            logger.info("Цей телефон уже в базі даних");
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
        return accountEntity;
    }

    public void update(AccountEntity accountEntity) {
        try {
            AccountEntityDAO<AccountEntity> accountEntityDAO = new AccountEntityDAOImpl();
            accountEntityDAO.update(accountEntity);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void delete(int id) {
        try {
            AccountEntityDAO<AccountEntity> accountEntityDAO = new AccountEntityDAOImpl();
            accountEntityDAO.delete(id);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void deleteByPhone(String phone) {
        try {
            AccountEntityDAO<AccountEntity> accountEntityDAO = new AccountEntityDAOImpl();
            accountEntityDAO.deleteByPhone(phone);
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }

    }

    public void deleteAll(){
        try {
            AccountEntityDAO<AccountEntity> accountEntityDAO = new AccountEntityDAOImpl();
            accountEntityDAO.deleteAll();
        } catch (SQLException e) {
            logger.error(e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }
}
