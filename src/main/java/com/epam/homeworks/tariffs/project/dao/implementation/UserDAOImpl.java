package com.epam.homeworks.tariffs.project.dao.implementation;

import com.epam.homeworks.tariffs.project.dao.UserDAO;
import com.epam.homeworks.tariffs.project.model.AccountEntity;
import com.epam.homeworks.tariffs.project.model.Tariff;
import com.epam.homeworks.tariffs.project.model.User;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;
import com.epam.homeworks.tariffs.project.services.AccountService;
import com.epam.homeworks.tariffs.project.services.TariffService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDAOImpl implements UserDAO {
    private static final String FIND_ALL = "SELECT * FROM users";
    private static final String DELETE = "DELETE FROM users WHERE id = ?";
    private static final String DELETE_BY_PHONE = "DELETE FROM users WHERE phone_number=?";
    private static final String DELETE_ALL = "DELETE FROM users";
    private static final String CREATE = "INSERT INTO users(phone_number, first_name, last_name, tariff_name) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE users SET phone_number = ?, first_name =?, last_name=?, tariff_name=? WHERE id = ?";
    private static final String FIND_BY_ID = "SELECT * FROM users WHERE id = ?";
    private static final String FIND_BY_NAME = "SELECT * FROM users WHERE first_name=? AND last_name=?";
    private static final String FIND_BY_PHONE = "SELECT * FROM users WHERE phone_number=?";


    private static Logger logger = LogManager.getLogger(UserDAOImpl.class);

    public User findByName(String firstName, String lastName) throws SQLException {
        User user = new User();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_NAME)) {
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            int id = resultSet.getInt("id");
            String first_name = resultSet.getString("first_name");
            String last_name = resultSet.getString("last_name");
            user.setId(id);
            user.setLast_name(last_name);
            user.setFirst_name(first_name);
        }
        return user;
    }

    public Optional<User> findByPhone(String phone) throws SQLException {
        User user = new User();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_PHONE)) {
            preparedStatement.setString(1, phone);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            int id = resultSet.getInt("id");
            String phone_number = resultSet.getString("phone_number");
            String first_name = resultSet.getString("first_name");
            String last_name = resultSet.getString("last_name");
            String tariff_name = resultSet.getString("tariff_name");
            user.setId(id);
            user.setLast_name(last_name);
            user.setFirst_name(first_name);
            AccountService accountService = new AccountService();
            if (accountService.findByPhone(phone_number).isPresent())
                user.setAccount(accountService.findByPhone(phone_number).get());
            TariffService tariffService = new TariffService();
            if (tariffService.findByName(tariff_name).isPresent())
                user.setTariff(tariffService.findByName(tariff_name).get());
        }
        return Optional.ofNullable(user);
    }

    public List<User> findAll() throws SQLException {
        List<User> userEntities = new ArrayList<>();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_ALL)) {
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String phone_number = resultSet.getString("phone_number");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                User user = new User();
                user.setFirst_name(first_name);
                user.setId(id);
                user.setLast_name(last_name);
                String tariff_name = resultSet.getString("tariff_name");
                userEntities.add(user);
            }
        }
        return userEntities;
    }

    public Optional<User> findById(int id) throws SQLException {
        User user = new User();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            int _id = resultSet.getInt("id");
            String phone_number = resultSet.getString("phone_number");
            String first_name = resultSet.getString("first_name");
            String last_name = resultSet.getString("last_name");
            String tariff_name = resultSet.getString("tariff_name");
            user.setId(_id);
            user.setLast_name(last_name);
            user.setFirst_name(first_name);
        }
        return Optional.ofNullable(user);
    }

    public User create(User entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(CREATE,
                Statement.RETURN_GENERATED_KEYS)) {
//            AccountService accountService = new AccountService();
//            entity.setAccount(accountService.create(entity.getAccount()));
//            TariffService tariffService = new TariffService();
//            entity.setTariff(tariffService.create(entity.getTariff()));
            preparedStatement.setString(1, entity.getAccount().getPhone_number());
            preparedStatement.setString(2, entity.getFirst_name());
            preparedStatement.setString(3, entity.getLast_name());
            preparedStatement.setString(4, entity.getTariff().getName());
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                resultSet.next();
                entity.setId(resultSet.getInt(1));
            }
        } catch (SQLIntegrityConstraintViolationException e) {
            logger.info(e.getMessage() + " " + e.getStackTrace());
        }
        return entity;
    }

    public void update(User entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getAccount().getPhone_number());
            preparedStatement.setString(2, entity.getFirst_name());
            preparedStatement.setString(3, entity.getLast_name());
            preparedStatement.setString(4, entity.getTariff().getName());
            preparedStatement.setInt(5, entity.getId());
            preparedStatement.executeUpdate();
        }
    }

    public int delete(int id) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }

   public int deleteAll() throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE_ALL)){
            return preparedStatement.executeUpdate();
        }
   }

    public void deleteByPhone(String phone) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE_BY_PHONE)) {
            preparedStatement.setString(1, phone);
            preparedStatement.execute();

        }
    }
}
