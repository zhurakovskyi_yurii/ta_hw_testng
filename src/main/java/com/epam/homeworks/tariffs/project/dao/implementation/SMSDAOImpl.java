package com.epam.homeworks.tariffs.project.dao.implementation;

import com.epam.homeworks.tariffs.project.dao.SMSDAO;
import com.epam.homeworks.tariffs.project.model.SMS;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SMSDAOImpl implements SMSDAO {

    private static final String FIND_ALL = "SELECT * FROM sms";
    private static final String DELETE = "DELETE FROM sms WHERE user_from = ?";
    private static final String CREATE = "INSERT INTO sms(`user_from`, `user_to`, `send_date`, `text`) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE sms SET user_from =?, user_to=?, text=?, date=? WHERE id=? ";
    private static final String FIND_BY_ID = "SELECT * FROM sms WHERE user_from = ?";

    public List<SMS> findAll() throws SQLException {
        List<SMS> smsEntities = new ArrayList<>();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_ALL)) {
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                SMS sms = new SMS();
                sms.setUser_from(resultSet.getInt("user_from"));
                sms.setUser_to(resultSet.getInt("user_to"));
                sms.setDate(resultSet.getDate("send_date"));
                sms.setText(resultSet.getString("text"));
                smsEntities.add(sms);
            }
        }
        return smsEntities;
    }

    public SMS findById(int user_from) throws SQLException {
        SMS sms = new SMS();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_ID)) {
            preparedStatement.setInt(1, user_from);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            sms.setUser_from(resultSet.getInt("user_from"));
            sms.setUser_to(resultSet.getInt("user_to"));
            sms.setDate(resultSet.getDate("send_date"));
            sms.setText(resultSet.getString("text"));
        }
        return sms;
    }

    public SMS create(SMS entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(CREATE,
                Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, entity.getUser_from());
            preparedStatement.setInt(2, entity.getUser_to());
            preparedStatement.setDate(3, entity.getDate());
            preparedStatement.setString(4, entity.getText());
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                resultSet.next();
                entity.setId(resultSet.getInt(1));
            }
        }
        return entity;
    }

    @Override
    public int deleteAll() throws SQLException {
        return 0;
    }

    public void update(SMS entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, entity.getUser_from());
            preparedStatement.setInt(2, entity.getUser_to());
            preparedStatement.setDate(3, entity.getDate());
            preparedStatement.setString(4, entity.getText());
            preparedStatement.setInt(5, entity.getId());
            preparedStatement.executeUpdate();
        }
    }

    public int delete(int user_from) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE)) {
            preparedStatement.setInt(1, user_from);
            return preparedStatement.executeUpdate();
        }
    }
}
