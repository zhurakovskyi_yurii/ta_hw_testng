package com.epam.homeworks.tariffs.project.dao.implementation;

import com.epam.homeworks.tariffs.project.dao.TariffDAO;
import com.epam.homeworks.tariffs.project.model.Tariff;
import com.epam.homeworks.tariffs.project.persistant.ConnectionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TariffDAOImpl implements TariffDAO {

    private static final String FIND_ALL = "SELECT * FROM tariffs";
    private static final String DELETE_BY_NAME = "DELETE FROM tariffs WHERE name = ?";
    private static final String DELETE = "DELETE FROM tariffs WHERE id=?";
    private static final String CREATE = "INSERT INTO tariffs (name, payroll, calls_on_net_cost, sms_within_ukraine_cost, internet, land_lines) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE tariffs SET `name` = ?, `payroll` = ?, `calls_on_net_cost` =?, `sms_within_ukraine_cost`=?, `internet`=?, `land_lines`=?  WHERE id = ?";
    private static final String FIND_BY_ID = "SELECT * FROM tariffs WHERE id = ?";
    private static final String FIND_BY_NAME = "SELECT * FROM tariffs WHERE name = ?";
    private static final String DELETE_ALL = "DELETE FROM tariffs";


    public List<Tariff> findAll() throws SQLException {
        List<Tariff> tariffEntities = new ArrayList<>();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_ALL)) {
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                double payroll = resultSet.getDouble("payroll");
                double calls_on_net_cost = resultSet.getDouble("calls_on_net_cost");
                double calls_land_lines = resultSet.getDouble("land_lines");
                double sms_within_ukraine_cost = resultSet.getDouble("sms_within_ukraine_cost");
                int internet_price = resultSet.getInt("internet");
                Tariff tariff = new Tariff();
                tariff.setName(name);
                tariff.setPayroll(payroll);
                tariff.setCalls_on_net_cost(calls_on_net_cost);
                tariff.setLand_lines(calls_land_lines);
                tariff.setSms_within_ukraine_cost(sms_within_ukraine_cost);
                tariff.setInternet(internet_price);
                tariffEntities.add(tariff);
            }
        }
        return tariffEntities;
    }

    public Optional<Tariff> findById(int id) throws SQLException {
        Optional<Tariff> tariffEntity;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            String name = resultSet.getString("name");
            double payroll = resultSet.getDouble("payroll");
            double calls_on_net_cost = resultSet.getDouble("calls_on_net_cost");
            double calls_land_lines = resultSet.getDouble("land_lines");
            double sms_within_ukraine_cost = resultSet.getDouble("sms_within_ukraine_cost");
            int internet_price = resultSet.getInt("internet");
            Tariff tariff = new Tariff();
            tariff.setName(name);
            tariff.setPayroll(payroll);
            tariff.setCalls_on_net_cost(calls_on_net_cost);
            tariff.setLand_lines(calls_land_lines);
            tariff.setSms_within_ukraine_cost(sms_within_ukraine_cost);
            tariff.setInternet(internet_price);
            tariffEntity = Optional.ofNullable(tariff);
        }
        return tariffEntity;
    }

    public Optional<Tariff> findByName(String name) throws SQLException {
        Optional<Tariff> tariffEntity;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(FIND_BY_NAME)) {
            preparedStatement.setString(1, name);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            String name1 = resultSet.getString("name");
            double payroll = resultSet.getDouble("payroll");
            double calls_on_net_cost = resultSet.getDouble("calls_on_net_cost");
            double calls_land_lines = resultSet.getDouble("land_lines");
            double sms_within_ukraine_cost = resultSet.getDouble("sms_within_ukraine_cost");
            int internet_price = resultSet.getInt("internet");
            Tariff tariff = new Tariff();
            tariff.setName(name1);
            tariff.setPayroll(payroll);
            tariff.setCalls_on_net_cost(calls_on_net_cost);
            tariff.setLand_lines(calls_land_lines);
            tariff.setSms_within_ukraine_cost(sms_within_ukraine_cost);
            tariff.setInternet(internet_price);
            tariffEntity = Optional.ofNullable(tariff);
        }
        return tariffEntity;
    }

    public Tariff create(Tariff entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(CREATE,
                Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setDouble(2, entity.getPayroll());
            preparedStatement.setDouble(3, entity.getCalls_on_net_cost());
            preparedStatement.setDouble(4, entity.getSms_within_ukraine_cost());
            preparedStatement.setInt(5, entity.getInternet());
            preparedStatement.setDouble(6, entity.getLand_lines());
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                resultSet.next();
                entity.setId(resultSet.getInt(1));
            }
        }
        return entity;
    }

    @Override
    public int deleteAll() throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE_ALL)) {
            return preparedStatement.executeUpdate();
        }
    }

    public void update(Tariff entity) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(UPDATE)) {
            preparedStatement.setDouble(1, entity.getLand_lines());
            preparedStatement.setInt(2, entity.getInternet());
            preparedStatement.setDouble(3, entity.getSms_within_ukraine_cost());
            preparedStatement.setDouble(4, entity.getCalls_on_net_cost());
            preparedStatement.setDouble(5, entity.getPayroll());
            preparedStatement.setString(6, entity.getName());
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        }
    }

    public void deleteByName(String name) throws SQLException {
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(DELETE_BY_NAME)) {
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
        }
    }
}
